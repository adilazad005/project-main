<?php 
require 'connect.php';
if(!isset($_SESSION['ADMIN_LOGIN'])){
	header("location:index.php");
	}
if(isset($_GET['type']) && $_GET['type']!=''){
	$type=$_GET['type'];
	if($type=='delete'){
		$id=$_GET['id'];
		$delete_sql="delete from counsellor where id='$id'";
		mysqli_query($con,$delete_sql);
	}
}

$sql="SELECT *FROM counsellor";
$result=mysqli_query($con,$sql);
?>
<!DOCTYPE html>
<html lang="en">
   <head>
    <title>Admin</title>
    <meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/glyphicon.css">
	<link rel="stylesheet" href="css/font-awesome.min.css">
	<link rel="stylesheet" href="css/style.css"/>	
   </head>
   <body>
        
         <div class="navbar">
		    <ul>
			   <li><a href="dashboard.php">Dashboard</a></li>
			   <li><a href="add_counsellor.php">Add Counsellor</a></li>
			   <li><a href="show_counsellor.php">Show Counsellor</a></li>
			      <div class="nav_right">
			        <?php if(isset($_SESSION['ADMIN_LOGIN'])){
						 echo "<li><a href='logout.php'> Logout</a></li>";
						   }else{
							  echo "<li><a href='index.php'>Login</a></li>"; 
						   }
					?>
			      </div>
			</ul>
		  </div>
        <div class="container">
			    <div class="user_table">
			      <table cellspacing="0"  class="datatable-1 table table-bordered table-striped display" id="table_id" width="100%">
	                        <thead>
                               <tr>
                                 <th style='text-align:center;'>S.No</th>
								 <th style='text-align:center;'>Username</th>
                                 <th style='text-align:center;'>Email</th>
								 <th style='text-align:center;'>Password</th>
								 <th style='text-align:center;'>Action</th>
                               </tr>
							</thead>
                            <tbody>
                        	<?php
							$cnt=1;
							while($arr=mysqli_fetch_assoc($result))
							{
                            ?>
							<tr>
				            <td align='center' style='font-size:13px;'><?php echo $cnt++?></td>
                            <td align='center' style='font-size:13px;'><?php echo $arr['name'];?></td>
                            <td align='center' style='font-size:13px;'><?php echo $arr['email'];?></td>
							<td align='center' style='font-size:13px;'><?php echo $arr['password'];?></td>
							<td>
								<span class='delete'><a href='?type=delete&id=<?php echo $arr['id']?>' onclick='return confirm("Are you sure to delete this?");'>Delete</a></span>
							   </td>
							</tr>
							
                            <?php } ?>   
                           </tbody>
                          </table>
				    </div>
		</div>
		
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
	<script>
     $(document).ready(function () {
        $('#table_id').DataTable();
     });
   </script>
  </body>
</html>