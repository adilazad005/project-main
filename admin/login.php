<?php 
require 'connect.php';
$msg = '';
if(isset($_POST['submit'])){
	$email = mysqli_real_escape_string($con,$_POST['email']);
	$password = mysqli_real_escape_string($con,$_POST['password']);
	$sql = "SELECT *FROM register WHERE email='$email' and password='$password'";
	$result=mysqli_query($con,$sql);
	$count=mysqli_num_rows($result);
	 if($count>0){
		$_SESSION['USER_LOGIN']='yes';
		$_SESSION['USER_EMAIL']=$email;
		header('location:index.php');
		die();
	   }
	 else
	 {
		 $msg="Please enter correct login details";
	 }
}
?>

<!DOCTYPE html>
<html lang="en">
   <head>
    <title>Registration Form</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/glyphicon.css">
	<link rel="stylesheet" href="css/font-awesome.min.css">
	<link rel="stylesheet" href="css/style.css"/>	
   </head>
   <body>
         <div class="navbar">
		    <ul>
			   <li><a href="index.php">Home</a></li>
			      <div class="nav_right">
			       <li><a href="login.php">Login</a></li>
			      </div>
			</ul>
		  </div>
    <div class="container">
	    <div class="row">
	        <div class="col-md-6 col-md-offset-3">
	            <div class="login-form">
                   <form action="" method="post">
                     <h2 class="text-center">Employee Login</h2> 
                     <div class="form-group">
                        <input type="text" class="form-control" name="email" placeholder="Email">
                     </div>
                     <div class="form-group">
                        <input type="password" class="form-control" name="password" placeholder="Password">
                     </div>        
                     <div class="form-group">
                        <input type="submit" name="submit" value="Login" class="btn_login">
                     </div>
			         <span style="color:red; margin-top:15px;"><?php echo $msg?></span>
                   </form>
                </div>
		    </div>
		</div>
	</div>
  </body>
</html>