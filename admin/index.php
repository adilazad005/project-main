<?php 
require 'connect.php';
if(isset($_SESSION['ADMIN_LOGIN'])){
	header("location:dashboard.php");
}
$msg = '';
if(isset($_POST['submit'])){
	$email = mysqli_real_escape_string($con,$_POST['email']);
	$password = mysqli_real_escape_string($con,$_POST['password']);
	$sql = "SELECT *FROM admin WHERE email='$email' and password='$password'";
	$result=mysqli_query($con,$sql);
	$count=mysqli_num_rows($result);
	 if($count>0){
		$row=mysqli_fetch_assoc($result);
		$_SESSION['ADMIN_LOGIN']=$email;
		$_SESSION['ADMIN_USERNAME']=$row['username'];
		header('location:dashboard.php');
		die();
	   }
	 else
	 {
		 $msg="Please enter correct login details";
	 }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>Admin Login</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
<style type="text/css">

.form-control {
    min-height: 41px;
    background: white;
    box-shadow: none !important;
    border-color: #e3e3e3;
}
.form-control:focus {
    border-color: #70c5c0;
}
.form-control, .btn {        
    border-radius: 2px;
}
.login-form {
    width: 350px;
    margin: 0 auto;
    padding: 100px 0 30px;      
}
.login-form form {
    color: #7a7a7a;
    border-radius: 2px;
    margin-bottom: 15px;
    font-size: 13px;
    background: #ececec;
    box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
    padding: 30px;  
    position: relative; 
}
.login-form h2 {
    font-size: 22px;
    margin: 35px 0 25px;
}
.login-form .avatar {
    position: absolute;
    margin: 0 auto;
    left: 0;
    right: 0;
    top: -50px;
    width: 95px;
    height: 95px;
    border-radius: 50%;
    z-index: 9;
    background: #70c5c0;
    padding: 15px;
    box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.1);
}
.login-form .avatar img {
    width: 100%;
}   
.login-form input[type="checkbox"] {
    position: relative;
    top: 1px;
}
.login-form .btn, .login-form .btn:active {        
    font-size: 16px;
    font-weight: bold;
    background: #70c5c0 !important;
    border: none;
    margin-bottom: 20px;
}
.login-form .btn:hover, .login-form .btn:focus {
    background: #50b8b3 !important;
}    
.login-form a {
    color: #fff;
    text-decoration: underline;
}
.login-form a:hover {
    text-decoration: none;
}
.login-form form a {
    color: #7a7a7a;
    text-decoration: none;
}
.login-form form a:hover {
    text-decoration: underline;
}
.login-form .bottom-action {
    font-size: 14px;
}
</style>
</head>
    <body>
      <div class="login-form">
         <form action="" method="post">
            <div class="avatar">
               <center><span style="font-size: 50px; color:white;">
               <i class="fa fa-user"></i></span></center>
            </div>
                <h2 class="text-center">Admin Login</h2> 
            <div class="form-group">
               <input type="text" class="form-control" name="email" placeholder="Email">
            </div>
            <div class="form-group">
              <input type="password" class="form-control" name="password" placeholder="Password">
            </div>        
            <div class="form-group">
              <button type="submit" name="submit" class="btn btn-primary btn-lg btn-block">Log in</button>
            </div>
			<span style="color:red; margin-top:15px;"><?php echo $msg?></span>
         </form>
      </div>
    </body>
</html>