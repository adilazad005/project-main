<?php 
require 'connect.php';
if(!isset($_SESSION['ADMIN_LOGIN'])){
	header("location:index.php");
	}
$msg = '';
if(isset($_POST['submit'])){
	$name = mysqli_real_escape_string($con,$_POST['name']);
	$email = mysqli_real_escape_string($con,$_POST['email']);
	$password = mysqli_real_escape_string($con,$_POST['password']);
	$sql = "INSERT INTO counsellor (name,email,password) values(
	'$name','$email','$password')";
	if(mysqli_query($con,$sql)){
		$msg = "Counsellor Successfully";
	}else{
		$msg = "Registration failed";
	}
}
?>
<!DOCTYPE html>
<html lang="en">
   <head>
    <title>Admin</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/style.css"/>	
   
   </head>
   <body>
        <div class="navbar">
		    <ul>
			   <li><a href="dashboard.php">Dashboard</a></li>
			   <li><a href="add_counsellor.php">Add Counsellor</a></li>
			   <li><a href="show_counsellor.php">Show Counsellor</a></li>
			      <div class="nav_right">
			        <?php if(isset($_SESSION['ADMIN_LOGIN'])){
						 echo "<li><a href='logout.php'> Logout</a></li>";
						   }else{
							  echo "<li><a href='index.php'>Login</a></li>"; 
						   }
					?>
			      </div>
			</ul>
		  </div>
        <div class="container">
		    <form method="post">
			    <h3 align="center">Add Counsellor</h3>
				<span style='color:green;'><?php echo $msg ?></span>
			    <div class="form-group">
				    <label>Username</label>
			        <input type="text"  name="name" class="form-control" required >		
				</div>
				<div class="form-group">
				    <label>Email</label>
			        <input type="email"  name="email" class="form-control" required >		
				</div>
			    <div class="form-group">
				    <label>Password</label>
				    <input type="password"  name="password" class="form-control" required >
			    </div>
				<input type="submit" name="submit" value="Saved Data"/>
			</form>
		</div>
  </body>
</html>