<?php 
require 'connect.php';
$condition='';
if(!isset($_SESSION['COUNSELLOR_LOGIN'])){
	header("location:index.php");
	}
if(isset($_GET['type']) && $_GET['type']!=''){
	$type=$_GET['type'];
	if($type=='status'){
		$operation=$_GET['operation'];
		$id=$_GET['id'];
		if($operation=='active'){
			$status='1';
		}
		else{
			$status='0';
		}
		$update_status="update users set status='$status',counsellor_id='".$_SESSION['COUNSELLOR_ID']."' where id='$id'";
		mysqli_query($con,$update_status);
	}
}
$sql="select *from users ";
$result=mysqli_query($con,$sql);
?>
<!DOCTYPE html>
<html lang="en">
   <head>
    <title>Dashboard</title>
    <meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/glyphicon.css">
	<link rel="stylesheet" href="css/font-awesome.min.css">
	<link rel="stylesheet" href="css/style.css"/>	
   </head>
   <body>
        
         <div class="navbar">
		    <ul>
			   <li><a href="dashboard.php">Dashboard</a></li>
			   <li><a href="claim.php">My Claim</a></li>
			      <div class="nav_right">
			        <?php if(isset($_SESSION['COUNSELLLOR_LOGIN'])){
						
						   }else{
							  echo "<li><a href='logout.php'>Logout</a></li>"; 
							  
						   }
					?>
			      </div>
			</ul>
		  </div>
        <div class="container">
		    <h1 align="center"><b>Welcome The The Counsellor</b></h1>
			    <div class="user_table">
				   <h2 align="center">Public Users</h2>
			      <table cellspacing="0"  class="datatable-1 table table-bordered table-striped display" id="table_id" width="100%">
	                        <thead>
                               <tr>
                                 <th style='text-align:center;'>S.No</th>
								 <th style='text-align:center;'>First Name</th>
								 <th style='text-align:center;'>Last Name</th>
                                 <th style='text-align:center;'>Email</th>
                                 <th style='text-align:center;'>Course</th>
								 <th style='text-align:center;'>Phone</th>
								 <th style='text-align:center;'>Claim</th>
                               </tr>
							</thead>
                            <tbody>
                        	<?php
							$cnt=1;
							while($arr=mysqli_fetch_assoc($result))
							{
                            ?>
							<tr>
				            <td align='center' style='font-size:13px;'><?php echo $cnt++?></td>
                            <td align='center' style='font-size:13px;'><?php echo $arr['f_name'];?></td>
							<td align='center' style='font-size:13px;'><?php echo $arr['l_name'];?></td>
                            <td align='center' style='font-size:13px;'><?php echo $arr['email'];?></td> 
                            <td align='center' style='font-size:13px;'><?php echo $arr['course']?></td>
							<td align='center' style='font-size:13px;'><?php echo $arr['phone'];?></td>
							<td>
							 <?php
								if($arr['status']==1){
									echo "<span class='badge-complete'><a href='?type=status&operation=deactive&id=".$arr['id']."'>Claim</a></span>&nbsp;";
								}else{
									echo "<span class='badge-pending'>Claim Success</span>&nbsp";
								}
								?>
							</td>
							</tr>
                            <?php } ?>   
                           </tbody>
                          </table>
				    </div>
		</div>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
	<script>
     $(document).ready(function () {
        $('#table_id').DataTable();
     });
   </script>
  </body>
</html>